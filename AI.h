#ifndef AI_H
#define AI_H
#include "Chess.h"
using namespace std;
class AI{
	public:
		AI(int (*func)(Chess *chess,int buffer[][4]));
		int choose(Chess *chess,int buffer[][4]);
	private:
		int (*choose_func)(Chess *chess,int buffer[][4]);

};
#endif
