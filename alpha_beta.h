#include "Chess_Eval_Func.h"
#include "Chess.h"
#include "Player.h"
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int alpha_beta_helper(Chess *chess, Player *max_player, int buffer[][4],int num_actions);  
int alpha_beta_search(Chess *chess, Player *player, int depth, int alpha, int beta, int maxim_player);

