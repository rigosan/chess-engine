#include "Player.h"	
#include "Chess.h"
Player::Player(int p, char color, int (*func) (Chess*,Player *player,int[][4],int num_actions) ) {
	set_color(color);
	set_moves(0);
	set_number(p);
	m_choose_func=func;
}
Player::~Player(){
	
}
int Player::set_enemy(Player *enemy_player){
	m_enemy_player=enemy_player;	
	return 0;
}
Player* Player:: copy(){

	Player *copy=new Player(m_number,m_color,m_choose_func);
	return copy; 
}

int Player::set_color(char color){
	
	m_color=color;
	return 0;
}

int Player::set_moves(int moves){

	m_moves=moves;
	return 0;
}

int Player::set_number(int number){

	m_number=number;
	return 0;
}
int Player:: set_kingloc(int r, int c){
	
	m_king_loc[0]=r;
	m_king_loc[1]=c;
	return 0;
}

char Player::get_color(){
	
	return	m_color;

}

int Player:: get_moves(){

	return	m_moves;
}
int Player:: get_number(){

	return	m_number;
}

Player* Player:: get_enemy(){

	return m_enemy_player;
}

int*  Player:: get_kingloc(int *buffer){
	buffer[0]=m_king_loc[0];
	buffer[1]=m_king_loc[1];
	return buffer;
}

