#ifndef CHESS_H
#define CHESS_H
#include "Board.h"
#include "Player.h"
#include <time.h>
#include <string.h>
using namespace std;
#define MOVES 3
#define ROW 1
#define COLUMN 2
#define PIECE 0
class Chess{

	public:
		
		//initialize Board, place all pawns and stuff, and place X's to represent empty spaces
		Chess(Player *one, Player *two, int randomize); //color1: the color for PLayer 1, (pieces start on the South side and start first 
								//color2: color for player 2, pieces start on North side
								//arg is are flags
		Player *m_player1;
		Player *m_player2;
		~Chess();
		int move_r(Player *player,int r, int c, int r2, int c2); //move char to location, must update colors array too and board
		int move_f(Player *player);
		//int actions(int r, int c); //takes, and [r,c] of piece. Returns -1 if piece is not there or -2 no moves available 	
		void represent();
		int get_meslpmoc(); // get the # of Moves Elapsed Since Last Pawn Move Or Capture, (checked every move to see if its a draw) 
		int set_meslpmoc(int m); //will call set_mslpmoc(0) every time a pawn moves or a capture is made
		int actions_r(Player *player,int coords[][4]); //takes in either 1, 2 player 1 or 2, and puts the coordinates of all pieces that can move  
		int display_actions(int buffer[][4], int num_actions);//Diplay moves in elegant, easy to see way
		int actions_f(Player *player, int coords[][4]);
		int pawn_promotion(Player *player); //after every pawn move must check if he has reached the end row and be promoted to Queen	
		Chess* copy(Player *one, Player *two);
		int increment_meslpmoc();
		int check_King(Player *player); //must check on every move that Kings is checked, when this is true, only move available will be ones
					//that make king unchecked, and if they cant uncheck themselves, then the game is over
	private:	
		Board *m_board;
		int piece(int r, int c,int buffer[]); //puts char of given piece coordinates into buffer[0] and player # who owns that piece into	
		int populate(); //place all the necessary chars to represent a chess Board. N are knights, R rooks, K kings and so on...
		int m_eslpmoc; //Moves Elapsed Since Last Pawn Move Or Capture (when determining a draw)
		
		int randomize();
		//all the helper methods to calculate the possible moves of a given piece, for every piece type
		int handle_Pawn(int r, int c,int coords[][4], Player *player, int counter,int piece_elem);
		int handle_Knight(int r, int c, int coords[][4], Player *player, int counter);
		int handle_Rook(int r, int c, int coords[][4], Player *player, int counter);
		int handle_King(int r, int c, int coords[][4], Player *player, int counter);
		int handle_Queen(int r, int c, int coords[][4], Player *player, int counter);
		int handle_Bishop(int r, int c, int coords[][4], Player *player, int counter);
		
};
#endif
