This project is about 60% finished.

It is a Chess playing program, meant to be run on the command line. It
utilizes a Board class, which makes a visual representation of the board using 
ascii chars so that it can be run on the terminal.
The Chess class uses a Board instance to create the chessboard and contains
all the code for the functions for the game. Has important functions such as
move(), actions() and represent(). Details are in the .hpp files. Everything is 
done and working as it should. But I have only made a human function for AI, so
it's a little boring just playing against myself if I dont have a partner.
Hence I will plan to make a MiniMax procedure to play against in AI
with a Evaluation function.


1.use "make" to compile. 
2. use "./chess" to run on the terminal
