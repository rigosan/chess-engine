#include "human.h"
int  human(Chess *chess,Player *player,int buffer[][4],int num_actions){
	int move_exists=0;		
	char from[256];
	int from_rc[2];	
	int elem_move=0;
	char to[256];
	int to_rc[2];
	
	/*10 chars should be enough to specify all the possible variations of r,c	
	for example:
			[r,c]  r,c  (r,c) r  ,  c  (r,c]
	*/	
again:
	
	printf("Give r,c pair of piece you want to move.\n");
	fgets(from,256,stdin);
	if(parse(from,from_rc)==-1){
		printf("Invalid input, please try again.\n");
		goto again;	
		}
	//printf("Piece to move:[%d,%d]\n",from_rc[0],from_rc[1]);
	//now must check if the move is valid (inside buffer)	
	move_exists=0;
	for(elem_move=0;elem_move<num_actions;elem_move++)
		if(buffer[elem_move][0]==from_rc[0]&& buffer[elem_move][1]==from_rc[1])
			move_exists=1;
	if(!move_exists){
		printf("Valid input, but piece cannot move or doesn't exist (or at least not yours).\n");
		goto again;	
		}
again2:	
	printf("Given piece is [%d,%c]. Move this piece where? Give r,c pair. Or enter q to go back.\n",from_rc[0],from_rc[1]+65);
	fgets(to,256,stdin);
	if(to[0]=='q')
		goto again;
	if(parse(to,to_rc)==-1){
		printf("Invalid input, please try again.\n");		
		goto again2;
		}
	move_exists=0;
	int chosen_move=-1;
	for(elem_move=0;elem_move<num_actions;elem_move++)
		if(buffer[elem_move][0]==from_rc[0] && buffer[elem_move][1]==from_rc[1] && buffer[elem_move][2]==to_rc[0]&& buffer[elem_move][3]==to_rc[1]){
			move_exists=1;
			chosen_move=elem_move;
			break;
			}
	if(!move_exists){
		printf("Valid input, but given piece cannot move here.\n");
		goto again2;	
		}
	printf("Will move [%d,%c] --->  [%d,%c]\n",from_rc[0],from_rc[1]+65,to_rc[0],to_rc[1]+65);	
	return chosen_move;

}
int parse(char *from, int from_rc[]){
        fseek(stdin,0,SEEK_END);	
	int num_coords=0;
	while(*from){
		if(*from >= 48 && *from<=57)
			if(num_coords==0){
				from_rc[0]=*from-48;
				num_coords++;
				}
			else
				return -1;		
		if(*from >=65 && *from <=90)
			if(num_coords==1){
				from_rc[1]=*from-65;
				num_coords++;
				}
			else
				return -1;	
		if(*from >=97 && *from <=122)
			if(num_coords==1){
				from_rc[1]=*from-97;
				num_coords++;
				}
			else
				return -1;	
		from++;
		}		
	if(num_coords!=2)
		return -1;
	return 0;
}
