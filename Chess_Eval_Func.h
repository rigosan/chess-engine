#include "Chess.h"
#include "Player.h"

//Evaluation function to give a value to a given board for a given player
//the higher the value the better the board for that player
int Chess_Eval_Func(Chess *chess, Player *player);

//Calculates sum of distance of all pieces to middle.
//the assumption is that controlling the middle is advantageuous
int dist_to_middle(Chess *chess, Player *player);

//calculates the piece values of pieces around the 
//vicinity of king to see if he's got protection
int king_protection(Chess *chess, Player *player);

//a simple sum of all the values for all non-captured pieces
//Pawn:100  Rook:500 Bishop: 500 
int sum_pieces(Chess *chess, Player *player);
int helpless_king(Chess *chess, Player *player);

