//a class for Player, simple containing information about a particular player like all his pieces location, moves, color, and possibly more
#ifndef PLAYER_H
#define PLAYER_H
using namespace std;
class Chess;
class Player{

	public:
		Player(int p,char color, int (*func) (Chess*,Player*,int[][4],int) ); //p player number, color is color representation for that player,  		
		~Player();
		int set_enemy(Player *enemy_player);
		int set_color(char color);	//	
		int set_moves(int moves); //would usually 
		int set_number(int number); 
		int set_kingloc(int r, int c); //it is useful to have the king's location saved because I have to check if the King is
						//checked when determining moves on each player
		char get_color(); 	//get the color for that player
		int get_moves();	//get the # of moves for that player
		int get_number();	//get the number for that player
		int* get_kingloc(int *buffer);
		int (*m_choose_func)(Chess*,Player *player,int[][4],int num_actions); //save the function that will be used to decide moves
		Player* get_enemy();
		int m_king_loc[2];
		Player* copy(); 	
		int **m_pieces; //information about pieces
		

	private:
		
		char m_color;
		int m_number;
		int m_moves;
		Player *m_enemy_player;
};
#endif
