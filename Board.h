//contains chess board class, simply contains the necessary stuff to represent a chessboard, none of the AI
//components present here.
#include <stdlib.h>
#include <stdio.h>
using namespace std;
class Board {
	public:
		Board(int r, int c);
		~Board();
		int place(int r, int c, char m, char color); //Places given char at (row,column) location, return 0 if successful, else nonzero.
		int remove(int r, int c); //removes whatever is at given location,places NULL, return 0 if successful, else nonzero
		char piece(int r, int c); //returns what is in the given location, might just be gibberish garbage if nothing has been placed there
		void represent(); //show the board in a board-like way
		char whatColor(int r, int c);
	private:
		int m_Nrows;//numero de rows
		int m_Ncolumns;//numero de columns
		char ** m_board;//array de chars para guardar todo lo que esta en la table
		char **m_colores;
		char m_letras[255]; //estos dos arrays son para representar la tabla en una forma elegante.
		int m_numeros[255];
};
