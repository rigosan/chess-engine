//implementation of Board
//
#include "Board.h"
#define NONE "\x1B[0m"
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define BLU "\x1B[34m"
Board::Board(int r,int c){

	m_Nrows=r;
	m_Ncolumns=c;
	m_board = new char*[r];
	m_colores= new char*[r];
	

	//para representar la tabla tengo que crear un array de 1...m_Nrows
	int num=0;
	
	for(int i=0; i<m_Nrows; i++){
		m_numeros[i]=num;
		num++; 
		}
	//Ascii integer for A to however many columns needed
 	char start=65;
	for(int i=0; i<m_Ncolumns ;i++){
		m_letras[i]=start;
		start++;
		}	

	for(int i=0; i<m_Nrows; i++){
		m_board[i]=new char[c];
		m_colores[i]=new char[c];
		for(int m=0; m<m_Ncolumns;m++){
			m_colores[i][m]='N';
			m_board[i][m]='\0';
			}
		}
}

Board::~Board(){
	
	for(int i=0; i<m_Nrows; i++){
		delete(m_board[i]);
		delete(m_colores[i]);
		}	
		
	delete(m_board);
	delete(m_colores);

}

int Board:: place(int r, int c, char m, char color){
	if(r<0 || r>=m_Nrows || c<0 || c>= m_Ncolumns)
		return -1;

	m_board[r][c]=m;
	m_colores[r][c]=color;	

	return 0;
}

int Board:: remove( int r, int c){

	if(r<0 || r>=m_Nrows || c<0 || c>= m_Ncolumns)
		return -1;
	m_board[r][c]=0;
	m_colores[r][c]=0;
	return 0;
}

char Board:: piece(int r, int c){
	if(r<0 || r>=m_Nrows || c<0 || c>= m_Ncolumns)
		return -1;


	return m_board[r][c];
}

char Board:: whatColor(int r, int c){
	
	if(piece(r, c)==-1)
		return -1;
	return m_colores[r][c];
	
}

void Board:: represent(){
	
	//just to print the "chess board" on the terminal semi-elegantly
	
	printf("\n   ");
	for(int i=0; i<m_Ncolumns; i++)
		printf(NONE "%c ",m_letras[i]);

	printf("\n");
	
	int index=0;
	for(int r=0; r<m_Nrows; r++){
		printf(NONE "\n%d  ",m_numeros[index]);
		for (int c=0; c< m_Ncolumns; c++){
			
			if(m_colores[r][c]=='R' || m_colores[r][c]=='r')
				printf(RED "%c ",m_board[r][c]);
			
			else if(m_colores[r][c]=='G' || m_colores[r][c]=='g')
				printf(GRN "%c ",m_board[r][c]);
			
			else if(m_colores[r][c]=='B' || m_colores[r][c]=='b')
				printf(BLU "%c ",m_board[r][c]);
			else if(m_board[r][c]=='\0')
				printf("  ");
			else 
				printf(NONE "%c ",m_board[r][c]);
				
			}
		printf(NONE " %d",m_numeros[index++]);
		
		}
	printf("\n");
	printf("\n   ");
	for(int i=0; i<m_Ncolumns; i++)
		printf(NONE "%c ",m_letras[i]);
	printf("\n");
	
}
