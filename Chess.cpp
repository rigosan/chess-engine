#include "Chess.h"

//printf("GOOOOES HEEEERE");

Chess:: Chess(Player *one, Player *two, int random ){

	m_player1=one;
	m_player2=two;
	
	m_player1->set_enemy(two);
	m_player2->set_enemy(one);
	
	set_meslpmoc(0); //moves elapsed since last pawn move or capture to 0	

	m_board=new Board(8,8);

	//16 because there are 16 different pieces for each player
	m_player1->m_pieces=new int*[16];
	m_player2->m_pieces=new int*[16];

	for(int i=0; i<16; i++){
		m_player1->m_pieces[i]=new int[4];
		m_player2->m_pieces[i]=new int[4];
		}
	if(random)
		randomize();
	else
		populate();
}
Chess* Chess:: copy(Player *one, Player *two){
	Chess *copy=new Chess(one,two,0);
	for(int i=0;i<16;i++){
		copy->m_player1->m_pieces[i][PIECE]=m_player1->m_pieces[i][PIECE];	
		copy->m_player1->m_pieces[i][ROW]=m_player1->m_pieces[i][ROW];	
		copy->m_player1->m_pieces[i][COLUMN]=m_player1->m_pieces[i][COLUMN];	
		copy->m_player1->m_pieces[i][MOVES]=m_player1->m_pieces[i][MOVES];	

		copy->m_player2->m_pieces[i][PIECE]=m_player2->m_pieces[i][PIECE];	
		copy->m_player2->m_pieces[i][ROW]=m_player2->m_pieces[i][ROW];	
		copy->m_player2->m_pieces[i][COLUMN]=m_player2->m_pieces[i][COLUMN];	
		copy->m_player2->m_pieces[i][MOVES]=m_player2->m_pieces[i][MOVES];	
	}
	for(int i=0;i<8;i++)
		for(int m=0;m<8;m++)	
			copy->m_board->place(i,m,m_board->piece(i,m),m_board->whatColor(i,m));
	copy->m_player1->set_kingloc(m_player1->m_king_loc[0],m_player1->m_king_loc[1]);
	copy->m_player2->set_kingloc(m_player2->m_king_loc[0],m_player2->m_king_loc[1]);

	copy->set_meslpmoc(get_meslpmoc());
	return copy;

}		
Chess:: ~Chess(){
	for(int i=0; i<16; i++){
		delete(m_player1->m_pieces[i]);
		delete(m_player2->m_pieces[i]);
		}
	delete(m_board);
	delete(m_player1);
	delete(m_player2);
}

int Chess:: populate(){
	char pieces[]="ppppppppRNBQKBNR";  //all the pieces necessary for a chess match, Pawn, Rook, kNight...

	int coords[]= {1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0};
	int coords2[]={0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};
	int boords[]= {6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7};
	
	//loops to populate m_player1 and m_player in order to initialize the board
	//Its true that the information about each piece on the board can be found
	//by searching each place iteratively using the board var from Board, but having an
	//2 arrays for each players pieces makes it much easier
	//Cada i se refiere a cada pieza, m_player# de jugador 1 o 2 
	for(int i=0; i<16; i++){
		m_player2->m_pieces[i][PIECE]=pieces[i];
		m_player2->m_pieces[i][ROW]=coords[i];
		m_player2->m_pieces[i][COLUMN]=coords2[i];
		m_player2->m_pieces[i][MOVES]=0;

		m_player1->m_pieces[i][PIECE]=pieces[i];
		m_player1->m_pieces[i][ROW]=boords[i];
		m_player1->m_pieces[i][COLUMN]=coords2[i];
		m_player2->m_pieces[i][MOVES]=0; }
	//hence: m_player[i][0] contains all the chars of the pieces,
	//ex. if its a pawn will have 'p' // and m_player[i][1] contains
	//the row number, and m_player[i][2] is column number



	for(int i=0; i<8;i++){
 		m_board->place(1,i, 'p',m_player2->get_color());
		m_board->place(6,i,'p',m_player1->get_color());
		}

	m_board->place(0,0,'R',m_player2->get_color());
	m_board->place(0,1,'N',m_player2->get_color());
	m_board->place(0,2,'B',m_player2->get_color());
	m_board->place(0,3,'Q',m_player2->get_color());
	m_board->place(0,4,'K',m_player2->get_color());
	m_board->place(0,5,'B',m_player2->get_color());
	m_board->place(0,6,'N',m_player2->get_color());
	m_board->place(0,7,'R',m_player2->get_color());

	m_board->place(7,0,'R',m_player1->get_color());
	m_board->place(7,1,'N',m_player1->get_color());
	m_board->place(7,2,'B',m_player1->get_color());
	m_board->place(7,3,'Q',m_player1->get_color());
	m_board->place(7,4,'K',m_player1->get_color());
	m_board->place(7,5,'B',m_player1->get_color());
	m_board->place(7,6,'N',m_player1->get_color());
	m_board->place(7,7,'R',m_player1->get_color());

	for (int r=2; r<6;r++)
		for(int c=0; c<8; c++)
			m_board->place(r,c,'x','N');
	m_player1->set_kingloc(7,4);	
	m_player2->set_kingloc(0,4);
	
	return 0;
}

//p is player number, 1 or 2 only. [r,c] is the coordinates of summoned piece. [i,m] is to where to move
int Chess:: move_r(Player *player, int r, int c, int i, int m){
	int s;
	int f;
	int e;
	int buffer [255][4];
	int no_enemy=1;
	int king_checked=0;
	int counter=0;
	int capture=0; //to check in the end if a capture has been made
	int not_there=1;
	int not_there2=1;
	
	for(s=0;s<16;s++)
		if(player->m_pieces[s][ROW]==r && player->m_pieces[s][COLUMN]==c){
			not_there=0;
			break;
			}
	if(not_there)
		return -2; //piece does not exist in that location

	switch(char piece=m_board->piece(r,c)){
		case 'p':
			counter=handle_Pawn(r,c,buffer,player,counter,s);
			break;
		case 'N':
			counter=handle_Knight(r,c,buffer,player,counter);
			break;

		case 'K':
			counter=handle_King(r,c,buffer,player,counter);
			break;

		case 'Q':
			counter=handle_Queen(r,c,buffer,player,counter);
			break;

		case 'R':
			counter=handle_Rook(r,c,buffer,player,counter);
			break;

		case 'B':
			counter=handle_Bishop(r,c,buffer,player,counter);
			break;
		case 'C':
			return -10; //error, cannot go here, tryint to move a captured

		}
	
	if(counter==0)
		return -3;//piece exists but cannot move at all, or its a captured piece
	for(f=0;f<counter;f++)
		if(buffer[f][2]==i && buffer[f][3]==m){
			not_there2=0;
			break;
			}
	if(not_there2)
		return -4;//given r,c piece can move, but not to the given destination i,m

	//Provided all the necessary checks namely that the given piece is able to nove, and
	//that the given destination is valid for that piece. now just make the necessary 
	//changes in the variables.

	if(player->m_pieces[s][PIECE]=='K')//if piece is a King, modify the king location for that piece
		player->set_kingloc(i,m);	
	m_board->remove(r,c);
	m_board->place(r,c,'x','N');
	player->m_pieces[s][ROW]=i;
	player->m_pieces[s][COLUMN]=m;
	if(m_board->piece(i,m)!='x'){ //dont check if its out of bounds because shouldve been already checked
		//if goes here then its a capture, must look for captured piece in enemy->pieces to 'C' it 	
		for(e=0;e<16;e++)
			if(player->get_enemy()->m_pieces[e][ROW]==i && player->get_enemy()->m_pieces[e][COLUMN]==m){
				no_enemy=0;
				break;
			}
		if(no_enemy)
			return -5; //error, m_board detected there was another piece in destination, but it wasnt found 
					//int enemies pieces

		player->get_enemy()->m_pieces[e][PIECE]='C';
		player->get_enemy()->m_pieces[e][ROW]=-1;
		player->get_enemy()->m_pieces[e][COLUMN]=-1;

		capture=1; //capture has been done, hence reset the moves elapsed since last pawn move or capture
		}	
	if(capture || player-> m_pieces[s][PIECE]=='p')
		set_meslpmoc(0);
	else 
		increment_meslpmoc();
	m_board->place(i,m,player->m_pieces[s][PIECE],player->get_color());	
	player->m_pieces[s][MOVES]++; //increment times this particular piece has moved
	return 0 ;

}
int Chess:: display_actions(int buffer[][4], int num_actions){
	for(int i=0;i<num_actions;i++)
		printf("Move %d:  [%d,%c] ---> [%d,%c]\n", i,buffer[i][0],buffer[i][1]+65,buffer[i][2],buffer[i][3]+65); 
		
		return 0;

	}
int Chess:: move_f(Player *player){
	int actions_buff[255][4];	
	int error=0;
	int num_actions=actions_f(player, actions_buff);
	if(num_actions==0)
		return -1; //PLayer has no moves available, end the game
	//player has at least one available move, hence still in the game

	//return element in actions_buff[] for the chosen move based on the choose_func already initialized by Player
	//display_actions(actions_buff,num_actions);
	int chosen_action=(player->m_choose_func)(this,player,actions_buff,num_actions); 

	//check to see if choose_func returned an invalid move
	if(chosen_action<0 || chosen_action>=num_actions){	
		printf("ERRORRR MADE INVALID MOVE:%d\n",error);
		exit(0);
		}
	//the choose_func has chosen a valid move, now actually make the move using move_r (move_raw)
	if((error=move_r(player,actions_buff[chosen_action][0],actions_buff[chosen_action][1],actions_buff[chosen_action][2],actions_buff[chosen_action][3])) <0)	{
		printf("ERRORRR MADE INVALID MOVE:%d\n",error);
		exit(0);
		}

	return 0;

}


void Chess:: represent(){
	m_board->represent();
}

// p refers to player, either 1 or 2. coords is a buffer of 2 dimensional arraw
int Chess:: actions_r(Player *player, int buffer[][4]){

	int counter=0; //too keep track of coordinates in coords

	for(int i=0; i<16; i++){

		switch(char piece=player->m_pieces[i][PIECE]){
			case 'p':
				counter=handle_Pawn(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter,i);
				break;
			case 'N':
				counter=handle_Knight(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter);
				break;
			case 'K':
				counter=handle_King(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter);
				break;
			case 'Q':
				counter=handle_Queen(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter);
				break;

			case 'R':
				counter=handle_Rook(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter);
				break;

			case 'B':
				counter=handle_Bishop(player->m_pieces[i][ROW],player->m_pieces[i][COLUMN],buffer, player,counter);
				break;

			}

		}
	return counter;
}
//this actions_full filters the moves from actions_r to get only moves that dont check king
//hence must create copies of Chess and Players, make the possible move and check if its checked
int Chess:: actions_f(Player *player, int buffer[][4] ){
	int newbuffer[255][4];
	int error;
	int ret_num=0;
	int player_num=player->get_number()-1;
	int enemy_num=player->get_enemy()->get_number()-1;

	int num_actions=actions_r(player,newbuffer);
	//got actions, now must check these moves dont cause king to be checked
	for(int i=0;i<num_actions;i++){
		Player *players[2];
		error=0;
		players[player_num]=player->copy();
		players[enemy_num]=player->get_enemy()->copy();
		Chess *chess_copy=copy(players[0],players[1]);	
		if( (error=chess_copy->move_r(players[player_num],newbuffer[i][0],newbuffer[i][1],newbuffer[i][2],newbuffer[i][3])) < 0){
			printf("Error with move_r:%d\n",error);
			exit(0);
			}
		if(chess_copy->check_King(players[player_num]))
				;
		else{
			buffer[ret_num][0]=newbuffer[i][0];
			buffer[ret_num][1]=newbuffer[i][1];
			buffer[ret_num][2]=newbuffer[i][2];
			buffer[ret_num][3]=newbuffer[i][3];						
			ret_num++;
			}

		}

	return ret_num;
}	
int Chess:: randomize(){
	srand(time(NULL));
	for (int r=0; r<8;r++)
		for(int c=0; c<8; c++)
			m_board->place(r,c,'x','N');
	


	char pieces[]="ppppppppRNBQKBNR";  //all the pieces necessary for a chess match, Pawn, Rook, kNight...
	int x=0,y=0;
	for(int i=0; i<16; i++){
again:	
			
		x=rand() % 8;
		y=rand() % 8;
		if(m_board->piece(x,y)!='x')
			goto again;
		if(pieces[i]=='K')
			m_player1->set_kingloc(x,y);	
		m_board->place(x,y,pieces[i],m_player1->get_color());
		m_player1->m_pieces[i][PIECE]=pieces[i];
		m_player1->m_pieces[i][ROW]=x;
		m_player1->m_pieces[i][COLUMN]=y;
		
		}	
	
	int a=0,b=0;
	for(int i=0; i<16; i++){
again2:		
		a=rand() % 8;
		b=rand() % 8;
		if(m_board->piece(a,b)!='x')
			goto again2;	
		if(pieces[i]=='K')
			m_player2->set_kingloc(a,b);	

		m_board->place(a,b,pieces[i],m_player2->get_color());
		m_player2->m_pieces[i][PIECE]=pieces[i];
		m_player2->m_pieces[i][ROW]=a;
		m_player2->m_pieces[i][COLUMN]=b;
		
		}	
	//Assume the game is 50 moves in
	m_player1->set_moves(50);
	m_player2->set_moves(50);


	return 0;
	


	}

int Chess:: handle_Pawn(int r , int c, int coords[][4], Player *player, int counter,int piece_elem){
	int addend=0;
	char color=player->get_color();
	if (player->get_number()==1)
		addend=-1;
	else if (player->get_number()==2)
		addend=1;
	else 
		; //should not be possible
		

	char kpiece=m_board->piece(r+addend, c);
	
	if(kpiece=='x'){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+addend;
		coords[counter++][3]=c;
		}
	if(kpiece=='x' && player->m_pieces[piece_elem][MOVES]==0 && m_board->piece(r+addend+addend,c)=='x' ){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+addend+addend;
		coords[counter++][3]=c;	
		}
	 	 




	kpiece= m_board->piece(r+addend,c+1);
	if(kpiece!='x' && kpiece!=-1 && m_board->whatColor(r+addend,c+1) !=color){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+addend;
		coords[counter++][3]=c+1;
		}
	kpiece= m_board->piece(r+addend,c-1);
	if(kpiece!='x' && kpiece !=-1 && m_board->whatColor(r+addend,c-1) !=color) {
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+addend;
		coords[counter++][3]=c-1;
		}

	return counter;			

}
int Chess:: pawn_promotion(Player *player){
	int last_rank;
	int piece[2];
	int i;
	if(player->get_number()==1)
		last_rank=7;
	else 
		last_rank=0;
	
	return 0;	
		
}

int Chess:: handle_Rook(int r , int c, int coords[][4], Player *player, int counter){
	char kpiece='\0';
	char kcolor='\0';
	char color=player->get_color();
	int i=1;
	while(1){    ///ROOK MOVEMENT FOR GOING RIGHT
		kpiece=m_board->piece(r,c+i);
		kcolor=m_board->whatColor(r,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){  //ROOK MOVEMENT FOR GOING LEFT
		kpiece=m_board->piece(r,c-i);
		kcolor=m_board->whatColor(r,c-i);

		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x')
			break;
		i++;
		}
	i=1;
	while(1){ //ROOK MOVEMENT FOR GOING UP
		kpiece=m_board->piece(r+i,c);
		kcolor=m_board->whatColor(r+i,c);
		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c;
		if(kcolor != color && kpiece!='x')
			break;
				
		i++;
		}
	i=1;
	while(1){//ROOK MOVEMENT FOR GOING DOWN
		kpiece=m_board->piece(r-i,c);
		kcolor=m_board->whatColor(r-i,c);

		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		
		}



	return counter;
}

int Chess:: handle_King(int r , int c, int coords[][4], Player *player, int counter){
	//king ability is similar to queen, except its just 1 possible space
	char kpiece='\0';
	char kcolor='\0';
	char color=player->get_color();
	
	    ///King MOVEMENT FOR GOING RIGHT ONE SPACE
	kpiece=m_board->piece(r,c+1);
	kcolor=m_board->whatColor(r,c+1);
	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c+1;
		}
	//King MOVEMENT FOR GOING LEFT ONE SPACE
	kpiece=m_board->piece(r,c-1);
	kcolor=m_board->whatColor(r,c-1);

	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c-1;
		}	
	//King MOVEMENT FOR GOING UP ONE SPACE
	kpiece=m_board->piece(r+1,c);
	kcolor=m_board->whatColor(r+1,c);
	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+1;
		coords[counter++][3]=c;
		}
				
	
	//KIng MOVEMENT FOR GOING DOWN ONE SPACE
	kpiece=m_board->piece(r-1,c);
	kcolor=m_board->whatColor(r-1,c);

	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-1;
		coords[counter++][3]=c;
		}	
	//King MOVEMENT FOR GOING diagonally down-right
	kpiece=m_board->piece(r+1,c+1);
	kcolor=m_board->whatColor(r+1,c+1);
	if(kpiece==-1 || color==kcolor)
		;
	else {
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+1;
		coords[counter++][3]=c+1;
		}
	
	//King MOVEMENT FOR GOING diagonally down-left
	kpiece=m_board->piece(r+1,c-1);
	kcolor=m_board->whatColor(r+1,c-1);
	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+1;
		coords[counter++][3]=c-1;
		}
	//King  MOVEMENT FOR GOING diagonally up-right
	kpiece=m_board->piece(r-1,c+1);
	kcolor=m_board->whatColor(r-1,c+1);
	if(kpiece==-1 || color==kcolor)
		;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-1;
		coords[counter++][3]=c+1;
		}
	//King MOVEMENT FOR GOING diagonally up-left
	kpiece=m_board->piece(r-1,c-1);
	kcolor=m_board->whatColor(r-1,c-1);
	if(kpiece==-1 || color==kcolor)
				;
	else{
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-1;
		coords[counter++][3]=c-1;
		}
	
		
		

	return counter;
}

int Chess:: handle_Knight(int r , int c, int coords[][4], Player *player, int counter){
	
	char kpiece ='\0';
	char kcolor='\0';
	char color=player->get_color();
	kpiece=m_board->piece(r+2, c+1);
	kcolor=m_board->whatColor(r+2,c+1);
	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+2;
		coords[counter++][3]=c+1;
		} 
	kpiece=m_board->piece(r+2, c-1);
	kcolor=m_board->whatColor(r+2,c-1);
	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+2;
		coords[counter++][3]=c-1;
		} 
	kpiece=m_board->piece(r-2, c+1);
	kcolor=m_board->whatColor(r-2,c+1);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r-2;
		coords[counter++][3]=c+1;
		} 
	
	kpiece=m_board->piece(r-2, c-1);
	kcolor=m_board->whatColor(r-2,c-1);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r-2;
		coords[counter++][3]=c-1;
		} 
	kpiece=m_board->piece(r+1, c+2);
	kcolor=m_board->whatColor(r+1,c+2);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+1;
		coords[counter++][3]=c+2;
		} 
	kpiece=m_board->piece(r-1, c+2);
	kcolor=m_board->whatColor(r-1,c+2);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		//printf("Printf [%d,%d]-->[%d,%d] with kpiece:%c\n with kcolor:%c and color:%c",r,c,r-1,c+2,kpiece,kcolor,color);
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r-1;
		coords[counter++][3]=c+2;
		} 
	kpiece=m_board->piece(r+1, c-2);
	kcolor=m_board->whatColor(r+1,c-2);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r+1;
		coords[counter++][3]=c-2;
		} 
	kpiece=m_board->piece(r-1, c-2);
	kcolor=m_board->whatColor(r-1,c-2);

	if( kpiece=='x' || (kpiece!=-1 && kcolor !=color)){
		coords[counter][0]=r;
		coords[counter][1]=c;		
		coords[counter][2]=r-1;
		coords[counter++][3]=c-2;
		} 


	return counter;
}

int Chess:: handle_Bishop(int r , int c, int coords[][4], Player *player, int counter){
	char kpiece='\0';
	char kcolor='\0';
	char color=player->get_color();
	int i=1;
	while(1){    //Bishop MOVEMENT FOR GOING diagonally down-right
		kpiece=m_board->piece(r+i,c+i);
		kcolor=m_board->whatColor(r+i,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Bishop MOVEMENT FOR GOING diagonally down-left
		kpiece=m_board->piece(r+i,c-i);
		kcolor=m_board->whatColor(r+i,c-i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Bishop MOVEMENT FOR GOING diagonally up-right
		kpiece=m_board->piece(r-i,c+i);
		kcolor=m_board->whatColor(r-i,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Bishop MOVEMENT FOR GOING diagonally up-left
		kpiece=m_board->piece(r-i,c-i);
		kcolor=m_board->whatColor(r-i,c-i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
		


	
	return counter;
}
int Chess:: handle_Queen(int r , int c, int coords[][4], Player *player, int counter){

	//queen is just the abilities of Rook and Bishop put together, so will reuse that code
	char kpiece='\0';
	char kcolor='\0';
	char color=player->get_color();
	int i=1;
	while(1){    ///queen MOVEMENT FOR GOING RIGHT
		kpiece=m_board->piece(r,c+i);
		kcolor=m_board->whatColor(r,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){  //QUeen MOVEMENT FOR GOING LEFT
		kpiece=m_board->piece(r,c-i);
		kcolor=m_board->whatColor(r,c-i);

		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x')
			break;
		i++;
		}
	i=1;
	while(1){ //Queen MOVEMENT FOR GOING UP
		kpiece=m_board->piece(r+i,c);
		kcolor=m_board->whatColor(r+i,c);
		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c;
		if(kcolor != color && kpiece!='x')
			break;
				
		i++;
		}
	i=1;
	while(1){//Queen MOVEMENT FOR GOING DOWN
		kpiece=m_board->piece(r-i,c);
		kcolor=m_board->whatColor(r-i,c);

		if(kpiece==-1 || color==kcolor)
			break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		
		}

	i=1;

	while(1){    //Queen MOVEMENT FOR GOING diagonally down-right
		kpiece=m_board->piece(r+i,c+i);
		kcolor=m_board->whatColor(r+i,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Queen MOVEMENT FOR GOING diagonally down-left
		kpiece=m_board->piece(r+i,c-i);
		kcolor=m_board->whatColor(r+i,c-i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r+i;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Queen MOVEMENT FOR GOING diagonally up-right
		kpiece=m_board->piece(r-i,c+i);
		kcolor=m_board->whatColor(r-i,c+i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c+i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
	i=1;
	while(1){    //Queen MOVEMENT FOR GOING diagonally up-left
		kpiece=m_board->piece(r-i,c-i);
		kcolor=m_board->whatColor(r-i,c-i);
		if(kpiece==-1 || color==kcolor)
				break;
		coords[counter][0]=r;
		coords[counter][1]=c;
		coords[counter][2]=r-i;
		coords[counter++][3]=c-i;
		if(kcolor != color && kpiece!='x'){
			break;
			}
		i++;
		}
		
	return counter;
}

int Chess:: set_meslpmoc(int s){
	m_eslpmoc=0;
	return 0;
}


int Chess:: get_meslpmoc(){
	return m_eslpmoc;
}

int Chess:: increment_meslpmoc(){
	m_eslpmoc++;
	return 0;
}

int Chess:: check_King(Player *player){
	
	//a simple way to check if player's King is checked is to 
	//get all enemy players actions and see if the King's location is in it
	int enemy_actions[255][4]; //will contain all the actions of enemy, its a buffer
	int king_loc[2];
	player->get_kingloc(king_loc);	
	

	int num_actions=actions_r(player->get_enemy(),enemy_actions);

	for(int i=0;i<num_actions;i++)
		if(enemy_actions[i][2]==king_loc[0] && enemy_actions[i][3]==king_loc[1])
			return 1;
	return 0;
}
int Chess:: piece(int r, int c, int buffer[]){
	buffer[0]=m_board->piece(r,c);
	
	if(buffer[0]==-1)//returns -1 if given coords not in board range
		return -1;	
	if(m_board->piece(r,c)==m_player1->get_color())
		buffer[1]=1;
	else
		buffer[1]=2; //if piece is within board and doesnt belong to p1, then belongs to p2	
	//of course if the piece was an empty space (an 'x') then doesnt matter what I put there
	//I expect the coder using these functions to check that the piece is an 'x' first	

	return 0;
}
