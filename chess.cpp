
#include "human.h"
#include "choose_random.h"
int main(int argc, char* argv[]) {
	
	Player *one=new Player(1,'b',choose_random);
	Player *two=new Player(2,'r',choose_random);
	Chess chess_board(one,two,0);

		
	chess_board.represent();
	
	srand(time(NULL));
	while(1){

		printf("\nPLAYER 1 to move\n");
		if(chess_board.move_f(one)==-1){
			printf("Player one has lost.\n");
			break;
			}
		
		chess_board.represent();
		printf("\nPLAYER 2 to move\n");
		if(chess_board.move_f(two)==-1){
			printf("Player two has lost.\n");
			break;	
			}

		if(chess_board.get_meslpmoc()/2>=50){
			printf("\nDRAW: 50 moves have passed since last pawn move or capture.\n");
			break;
			}
		chess_board.represent();
		}
	
	return 0;

}
