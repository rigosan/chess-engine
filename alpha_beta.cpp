#include "alpha_beta.h"

int alpha_beta_helper(Chess *chess, Player *max_player, int buffer[][4], int num_actions){
	int value=-999999;
	int temp=0;
	int best_index=0;
	int player_num=max_player->get_number()-1;
        int enemy_num=max_player->get_enemy()->get_number()-1;
	int i;
	for( i=0;i<num_actions;i++){
		Player *players[2];
                players[player_num]=max_player->copy();
                players[enemy_num]=max_player->get_enemy()->copy();
                Chess *chess_copy=chess->copy(players[0],players[1]);
               	if( chess_copy->move_r(players[player_num],buffer[i][0],buffer[i][1],buffer[i][2],buffer[i][3]) < 0){
                       	printf("Error in Alpha Beta search with move_rwith move_r:\n");
			exit(1);
			}
		if((temp=alpha_beta_search(chess_copy,players[enemy_num], 5,-9999999,9999999,0))>value){
			value=temp;
			best_index=i;
			}
		}	

	return i;	

}
int alpha_beta_search(Chess *chess, Player *player, int depth, int alpha, int beta, int maxim_player){
	int newbuffer[255][4];
	int value;
	int num_actions;
	int player_num=player->get_number()-1;
        int enemy_num=player->get_enemy()->get_number()-1;
	if(depth==0 || (num_actions=chess->actions_f(player,newbuffer))==-1)
		return Chess_Eval_Func(chess,player);
	if(maxim_player){
		value=-9999999;	
	        for(int i=0;i<num_actions;i++){
			Player *players[2];
                	players[player_num]=player->copy();
                	players[enemy_num]=player->get_enemy()->copy();
                	Chess *chess_copy=chess->copy(players[0],players[1]);
                	if( chess_copy->move_r(players[player_num],newbuffer[i][0],newbuffer[i][1],newbuffer[i][2],newbuffer[i][3]) < 0){
                        	printf("Error in Alpha Beta search with move_rwith move_r:\n");
				exit(1);
				}
			value=MAX(value,alpha_beta_search(chess_copy,players[enemy_num], depth-1,alpha,beta,0)); 
			alpha=MAX(alpha,value);
			if(alpha>beta)
				break;
                	}
		return value;
		}	
	else{
		value=999999;
		for(int i=0;i<num_actions;i++){
			Player *players[2];
                	players[player_num]=player->copy();
                	players[enemy_num]=player->get_enemy()->copy();
                	Chess *chess_copy=chess->copy(players[0],players[1]);
                	if( chess_copy->move_r(players[player_num],newbuffer[i][0],newbuffer[i][1],newbuffer[i][2],newbuffer[i][3]) < 0){
                        	printf("Error in Alpha Beta search with move_rwith move_r:\n");
				exit(1);
				}
			value=MIN(value,alpha_beta_search(chess_copy,players[enemy_num], depth-1,alpha,beta,1)); 
			beta=MIN(beta,value);	
			if(alpha>beta)
				break;
			}	
		return value;
		}


}
